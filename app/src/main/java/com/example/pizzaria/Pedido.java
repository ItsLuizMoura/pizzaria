package com.example.pizzaria;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Pedido extends AppCompatActivity {

    private TextView refName;
    private TextView refAddress;
    private TextView refPhone;
    private TextView refPizzaSize;
    private TextView refFlavours;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        refName = (TextView) findViewById(R.id.refName);
        refAddress = (TextView) findViewById(R.id.refAddress);
        refPhone = (TextView) findViewById(R.id.refPhone);
        refPizzaSize = (TextView) findViewById(R.id.refPizzaSize);
        refFlavours = (TextView) findViewById(R.id.refFlavours);

        refName.setText("Nome: " + getIntent().getStringExtra("insertName"));
        refPhone.setText("Telefone: " + getIntent().getStringExtra("insertPhone"));
        refAddress.setText("Endereço: " + getIntent().getStringExtra("insertAddress"));
        refFlavours.setText("Sabor(es): " + getIntent().getStringExtra("pizzaFlavours"));

        int pizzaSize = Integer.parseInt(getIntent().getStringExtra("pizzaSize"));

        if(pizzaSize == 1 ){
            refPizzaSize.setText("Pizza pequena");
        }
        else if(pizzaSize == 2){
            refPizzaSize.setText("Pìzza média");
        }
        else{
            refPizzaSize.setText("Pizza grande");
        }
    }

}
