package com.example.pizzaria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.pizzaria.Objects.Pizza;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public int pizzaSize = 0;
    List<Pizza> Pizzas = new ArrayList<>();

    private EditText insertName;
    private EditText insertPhone;
    private EditText insertAddress;

    private RadioButton radioPizzaPequena;
    private RadioButton radioPizzaMedia;
    private RadioButton radioPizzaGrande;

    private CheckBox checkCalabresa;
    private CheckBox check5Queijos;
    private CheckBox checkPortuguesa;
    private CheckBox checkMarguerite;
    private CheckBox checkDaCasa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioPizzaPequena = (RadioButton) findViewById(R.id.radioPizzaPequena);
        radioPizzaMedia = (RadioButton) findViewById(R.id.radioPizzaMedia);
        radioPizzaGrande = (RadioButton) findViewById(R.id.radioPizzaGrande);

        insertName = (EditText) findViewById(R.id.insertName);
        insertPhone = (EditText) findViewById(R.id.insertPhone);
        insertAddress = (EditText) findViewById(R.id.insertAddress);

        checkPortuguesa = (CheckBox) findViewById(R.id.checkPortuguesa);
        check5Queijos = (CheckBox) findViewById(R.id.check5Queijos);
        checkCalabresa = (CheckBox) findViewById(R.id.checkCalabresa);
        checkMarguerite = (CheckBox) findViewById(R.id.checkMarguerite);
        checkDaCasa = (CheckBox) findViewById(R.id.checkDaCasa);
    }

    public void pizzaSizeClick(View v){

        checkPortuguesa.setChecked(false);
        check5Queijos.setChecked(false);
        checkCalabresa.setChecked(false);
        checkMarguerite.setChecked(false);
        checkDaCasa.setChecked(false);

        Pizzas.removeAll(Pizzas);

        if(radioPizzaPequena.isChecked()){
            pizzaSize = 1;
        }
        else if(radioPizzaMedia.isChecked()){
            pizzaSize = 2;
        }
        else if(radioPizzaGrande.isChecked()) {
            pizzaSize = 3;
        }
    }

    //#Region On *PIZZA* Selected

    public void onPortuguesaSelected(View v){
        if(checkPortuguesa.isChecked()){
            if(checkFlavourAvailability()){
                Pizza pizza = new Pizza();
                pizza.setId(1);
                pizza.setNome("Portuguesa");
                Pizzas.add(pizza);
            }
            else{
                checkPortuguesa.setChecked(false);
            }
        }
        else{
            removePizzaById(1);
        }
    }

    public void onCalabresaSelected(View v){
        if(checkCalabresa.isChecked()){
            if(checkFlavourAvailability()){
                Pizza pizza = new Pizza();
                pizza.setId(2);
                pizza.setNome("Calabresa");
                Pizzas.add(pizza);
            }
            else{
                checkCalabresa.setChecked(false);
            }
        }
        else{
            removePizzaById(2);
        }
    }

    public void on5QueijosSelected(View v){
        if(check5Queijos.isChecked()){
            if(checkFlavourAvailability()){
                Pizza pizza = new Pizza();
                pizza.setId(3);
                pizza.setNome("5Queijos");
                Pizzas.add(pizza);
            }
            else{
                check5Queijos.setChecked(false);
            }
        }
        else{
            removePizzaById(3);
        }
    }

    public void onMargueriteSelected(View v){
        if(checkMarguerite.isChecked()){
            if(checkFlavourAvailability()){
                Pizza pizza = new Pizza();
                pizza.setId(4);
                pizza.setNome("Marguerite");
                Pizzas.add(pizza);
            }
            else{
                checkMarguerite.setChecked(false);
            }
        }
        else{
            removePizzaById(4);
        }
    }

    public void onDaCasaSelected(View v){
        if(checkDaCasa.isChecked()){
            if(checkFlavourAvailability()){
                Pizza pizza = new Pizza();
                pizza.setId(5);
                pizza.setNome("DaCasa");
                Pizzas.add(pizza);
            }
            else{
                checkDaCasa.setChecked(false);
            }
        }
        else{
            removePizzaById(5);
        }
    }

    //#Endregion

    public boolean checkFlavourAvailability(){

        switch(pizzaSize){
            case 1:
                if(Pizzas.size() < 1){
                    return true;
                }
                else{
                    broadcastMessage("Você já selecionou o número máximo de sabores.");
                    return false;
                }

            case 2:
                if(Pizzas.size() < 2){
                    return true;
                }
                else{
                    broadcastMessage("Você já selecionou o número máximo de sabores.");
                    return false;
                }

            case 3:
                if(Pizzas.size() < 3){
                    return true;
                }
                else{
                    broadcastMessage("Você já selecionou o número máximo de sabores.");
                    return false;
                }

            default:
                broadcastMessage("Você deve selecionar o tamanho da pizza primeiro.");
                return false;
        }
    }

    public void broadcastMessage(String message){
        Toast toast = Toast.makeText(getApplicationContext(),
                message,
                Toast.LENGTH_LONG);
        toast.show();
    }

    public void removePizzaById(int id){
        for(Pizza pizza: Pizzas){
            if (pizza.getId() == id){
                Pizzas.remove(pizza);
                break;
            }
        }
    }

    public void continueToOrder(View view) {

        String name = insertName.getText().toString();
        String address = insertAddress.getText().toString();
        String phone = insertPhone.getText().toString();
        String pizzaSizeStr = Integer.toString(pizzaSize);
        String pizzaFlavours = "";

        for(Pizza pizza : Pizzas) pizzaFlavours += pizza.getNome() + " ";

        if(!name.equals("")){
            if(!phone.equals("")){
                if(!address.equals("")){
                    if(pizzaSize != 0 && Pizzas.size() > 0){
                        Intent intent = new Intent(this, Pedido.class)
                                .putExtra("insertName", name)
                                .putExtra("insertPhone", phone)
                                .putExtra("insertAddress", address)
                                .putExtra("pizzaFlavours", pizzaFlavours)
                                .putExtra("pizzaSize", pizzaSizeStr);
                        startActivity(intent);
                    }
                    else{
                        broadcastMessage("Você deve selecionar ao menos uma pizza");
                    }
                }
                else{
                    broadcastMessage("Você deve preencher o campo Endereço");
                }
            }
            else{
                broadcastMessage("Você deve preencher o campo Telefone");
            }
        }
        else{
            broadcastMessage("Você deve preencher o campo Nome");
        }
    }
}
